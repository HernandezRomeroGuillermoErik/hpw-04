function crear_input(tipo){
 var obj_temporal=document.createElement('input');
 obj_temporal.type=tipo;
 document.body.appendChild(obj_temporal);
 return obj_temporal;
 }
 
 
function crear_input_password(tipo,placeholder){
 var obj_temporal=document.createElement('input');
 obj_temporal.type=tipo;
 obj_temporal.placeholder=placeholder;
 document.body.appendChild(obj_temporal);
 return obj_temporal;
}

 function crear_radio(tipo){
 var obj_temporal=document.createElement('input');
 obj_temporal.type=tipo;
 document.body.appendChild(obj_temporal);
 return obj_temporal;
}

function crear_sel(tipo,v){
 var obj_temporal=document.createElement('input');
 obj_temporal.type=tipo;
 obj_temporal.checked=v;
 document.body.appendChild(obj_temporal);
 return obj_temporal;
}



function crear_textarea(){
  var obj_temporal=document.createElement('textarea');
  document.body.appendChild(obj_temporal);
  return obj_temporal;
}

function crear_number(tipo,min,max,step){
  var obj_temporal=document.createElement('input');
  document.body.appendChild(obj_temporal);
  obj_temporal.type=tipo;
  obj_temporal.min=min;
  obj_temporal.max=max;
  obj_temporal.step=step;
  return obj_temporal;
}


function crear_checbox(tipo){
  var obj_temporal=document.createElement('input');
  obj_temporal.type=tipo;
  document.body.appendChild(obj_temporal);
  return obj_temporal;  
}


function crear_progress(tipo){
  var obj_temporal=document.createElement('progress');
  obj_temporal.type=tipo;
  document.body.appendChild(obj_temporal);
  return obj_temporal;   
}

function input_color(color){
   var obj_temporal=document.createElement('input');
   obj_temporal.type="color";
   obj_temporal.value=color;
   document.body.appendChild(obj_temporal);
   return obj_temporal;   
}

function crear_lista()
{
    var tipo= document.createElement("select");
    var opg= document.createElement("optgroup");
    opg.label="elementos de la lista";
    var op=document.createElement("option");
    var op2=document.createElement("option")
    var texto1=document.createTextNode("elemento1");
    var texto2=document.createTextNode("elemento1");
    op.appendChild(texto1);
    op2.appendChild(texto2)
    opg.appendChild(op);
    opg.appendChild(op2);
    tipo.appendChild(opg);
    
    document.body.appendChild(tipo);
}




function crear_fieldset()
{
    var fiel=document.createElement("fieldset");
    var legend=document.createElement("legend");
    var lab= document.createElement("label");
    var input= document.createElement("input");
    var texto_leyenda= document.createTextNode("Legend");
    var texto=document.createTextNode("nombre");
    lab.for="field";
    input.type="text";
    legend.appendChild(texto_leyenda);
    lab.appendChild(texto);
    fiel.appendChild(legend);
    fiel.appendChild(lab)
    fiel.appendChild(input);
    
    document.body.appendChild(fiel);
    
}


crear_input("text");
crear_input_password("password","password");
crear_textarea();
crear_radio("radio");
crear_sel("radio","true");
crear_checbox("checkbox");
crear_sel("checkbox","true")
crear_progress("progress");
crear_input("date");
crear_input("month");
crear_input("week");
crear_input("time");
crear_input("datetime");
crear_input("datetime-local");
crear_lista();
input_color("#e76252");
input_color("#000000");
crear_fieldset();
crear_input_password("placeholder","placeholder");
crear_input("file");
crear_input("submit");
crear_input("email");
crear_input("tel");
crear_input("url");
crear_input("number");
crear_input("search");
crear_number("number","0","15","3");
crear_input("range");
crear_number("range","0","15","3");

 



